var express = require('express');
var router = express.Router();
const echoService= require('../services/echo-service')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'HTickets-Rooms API' });
});

router.post('/echo', async function(req, res, next) {
  res.json(await echoService.echo(req.body.data));
});


module.exports = router;
