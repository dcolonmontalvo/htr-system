const topicName = process.env.IN_TOPIC || 'out.htrooms';
const socketName = process.env.OUT_SOCKET || 'htrooms';

class EchoListener {

    constructor(kafkaAdapter,socketAdapter){
        console.debug(kafkaAdapter.getInfo());
        console.debug(socketAdapter.getInfo());
        this.kafka=kafkaAdapter;
        this.socket=socketAdapter;
    }

   sendResponseToSocket(data){
       if(this.socket){
         this.socket.sendNotification(socketName,data);
         console.debug('Sent using socket:' + JSON.stringify(data));
       }else{
          console.error("Socket is not ready."); 
       }
    }

    async listen(){
        console.debug("Iniciando subscripcion a " + topicName);
        await this.kafka.suscribeToTopic(topicName, (data)=>{
            console.debug("Ejecutando callback", data);
            if(this.socket){
                let message= JSON.parse(data.message);
                this.socket.sendNotification(socketName,message);
                console.debug('Sent using socket:' + JSON.stringify(message));
              }else{
                 console.error("Socket is not ready."); 
              }
        });
        console.debug("Subscrito a " + topicName);
    }

}


module.exports.getInstance = (kafka,socket) => { return new EchoListener(kafka,socket)};