const kafkaAdapter= require('../adapters/kafka-adapter').getInstance();
const topicName = process.env.OUT_TOPIC || 'in.htrooms';

async function emitEcho(message){
   let kmessage={ data: message};
   await kafkaAdapter.sendToTopic(topicName,JSON.stringify(kmessage));
   return { success: true, status: 'pending'};
}


module.exports={
    echo: emitEcho
}