const { Kafka } = require('kafkajs')
const kafkaServer= process.env.KAFKA_SERVER || 'localhost:9091';
const kafkaClientId= process.env.KAFKA_CLIENT_ID || 'api-service-local';
let instance;

class KafkaAdapter{

     constructor(){
         this.kafka =  new Kafka({
            clientId: kafkaClientId,
            brokers: [kafkaServer],
            connectionTimeout: 3000
        });   
    }

    async suscribeToTopic(topicName, callback){
        if(!this.consumer){
            this.consumer = this.kafka.consumer({ groupId: kafkaClientId });
            await this.consumer.connect();
        }

        await this.consumer.subscribe({ topic: topicName, fromBeginning: true });
        await this.consumer.run({
            eachMessage: async ({ topic, partition, message }) => {
                console.debug('-'+ message.key+'#'+message.value)
                var data={
                    message: message.value.toString(), 
                    key: message.key ? message.key.toString():'',
                    topic:topic,
                    partition:partition};
                console.debug(JSON.stringify(data));
                callback(data);
            },
        })
    }

    async sendToTopic(topicName, message){
        console.debug("Sending msg: " + message + ", to:" + topicName);
        if(!this.producer){
            this.producer = this.kafka.producer()
            await this.producer.connect();
        }

        await this.producer.send({
            topic: topicName,
            messages: [
              { value: message },
            ],
          });
    }

    getInfo(){ return "KafkaAdapter"};

}

module.exports.getInstance= () => {
    if(!instance)
        instance = new KafkaAdapter();
    return instance;    
};