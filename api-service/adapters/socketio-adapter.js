let instance;
class SocketNotifier{

    constructor(socket){
        this.setSocket(socket);
      this.status = 'Assembled';
      console.debug("Socket assembled");
    }

    async sendNotification(channel,data){
        if(!this.socket){
            throw new Error("Socket not found!");
        }
        this.socket.emit(channel,data);
    }

    setSocket(socketId){
        this.socket = socketId;
    }

    addChannelListener(channel, callback){
        console.info("listener added");
        this.socket.on(channel, msg=>callback(msg));
    }

    getInfo(){ return "SocketIOAdapter"};

}

module.exports.create =  function(io){ 
   instance= new SocketNotifier(io);
   return instance;
}