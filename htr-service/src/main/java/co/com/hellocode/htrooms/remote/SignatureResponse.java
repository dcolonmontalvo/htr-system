package co.com.hellocode.htrooms.remote;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SignatureResponse {
    private String id;
    private String value;
    private String sign;
    private String signAt;
}
