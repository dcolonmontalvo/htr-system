 package co.com.hellocode.htrooms.listeners;


import co.com.hellocode.htrooms.remote.EchoRequest;
import co.com.hellocode.htrooms.remote.SignatureResponse;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaProducerException;
import org.springframework.kafka.core.KafkaSendCallback;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@Service
public class EchoListener {

    public static final int SIGNATURE_SIZE = 20;
    private final Gson g = new Gson();


    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Value("${htrservice.kafka.publisherTopic}")
    private String outTopic;


    @KafkaListener(topics ="${htrservice.kafka.consumerTopic}",
            containerFactory
                    ="kafkaListenerContainerFactory",groupId
            ="${htrservice.kafka.appId}", properties =
            {"max.poll.interval.ms:5000",
                    "max.poll.records:100"})
    public void listenSignature(List<ConsumerRecord<String, String>> records){
        log.info("listenSignature:{}",records.toString());
        try {
            process(records);
        }catch (Exception exc){
            log.error(exc.getMessage(),exc);
        }
    }

    private void process(List<ConsumerRecord<String, String>> records) {
        records.parallelStream()
                .peek(t->log.debug("Message:{}",t.value().toString()))
                .map(this::toEchoRequest)
                .peek(t->log.debug("Request:{}",t))
                .map(this::addSignature)
                .peek(t->log.debug("Response:{}",t))
                .map(g::toJson)
                .peek(t->log.debug("Json:{}",t))
                .forEach(this::responseMessage);
    }

    private void responseMessage(String json){
        ListenableFuture<SendResult<String, String>> future=
                kafkaTemplate.send(outTopic,json);

        future.addCallback(new KafkaSendCallback<String, String>() {
            @Override
            public void onSuccess(SendResult<String, String> result) {
                log.info("Message sent:{}", result.getProducerRecord().value());
            }

            @Override
            public void onFailure(Throwable ex) {
                log.error("Error Message:{},{} ", ex.getMessage());
            }

            @Override
            public void onFailure(KafkaProducerException ex) {
                log.error("Error Message:{},{} ", ex.getMessage());

            }
        });

    }

    private SignatureResponse addSignature(EchoRequest request){
        String code= RandomStringUtils.random(SIGNATURE_SIZE, Boolean.TRUE, Boolean.TRUE);;
        return SignatureResponse.builder()
                .id(request.getId())
                .sign(code)
                .value(request.getData())
                .signAt(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME))
                .build();
    }


    private EchoRequest toEchoRequest(ConsumerRecord<String,String> record){

        return g.fromJson(record.value(), EchoRequest.class);
    }


}
