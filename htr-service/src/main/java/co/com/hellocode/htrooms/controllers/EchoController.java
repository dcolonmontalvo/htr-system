package co.com.hellocode.htrooms.controllers;

import co.com.hellocode.htrooms.remote.EchoRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EchoController {

    @PostMapping(path = "/echo",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces =MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<String> getEcho(@RequestBody EchoRequest request){

        return ResponseEntity.accepted().body("Echo: " + request.getData());

    }

}
