package co.com.hellocode.htrooms.remote;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class EchoRequest {

    private String id;
    private String data;

}
